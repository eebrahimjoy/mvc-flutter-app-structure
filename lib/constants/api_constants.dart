const int kTokenExpired = 401;

// ********************** TEST SERVER ****************************

const apiBaseUrl = 'https://cdn.jsdelivr.net/gh/fawazahmed0/quran-api';

// ********************** LIVE SERVER ****************************

const String kEditionsApiEndPoint = '$apiBaseUrl/editions.json';
