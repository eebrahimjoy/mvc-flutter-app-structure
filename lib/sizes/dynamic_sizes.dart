import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

final kHeight = Get.size.height;
final kWidth = Get.size.width;
final kSearchBoxHeight = kHeight * 0.04;
final kSmallHeight = kHeight * 0.005;
final kMediumHeight = kHeight * 0.01;
final kSmallWidth = kWidth * 0.01;
final kMediumWidth = kWidth * 0.02;

final kSmallIcon = kHeight * 0.022;


final kSmallFontSize = kHeight * 0.015;

final kSmallSizedBoxWidth = SizedBox(width: kSmallWidth);
final kMediumSizedBoxWidth = SizedBox(width: kMediumWidth);
