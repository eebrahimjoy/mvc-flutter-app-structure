import 'dart:io';
import 'package:dio/dio.dart';
import 'package:mvc_structure_flutter/constants/api_constants.dart';
import 'package:mvc_structure_flutter/constants/pref_constants.dart';
import 'package:mvc_structure_flutter/helper/app_helper.dart';
import 'package:mvc_structure_flutter/helper/pref_helper.dart';
import 'package:mvc_structure_flutter/networks/api_exception.dart';
import 'package:mvc_structure_flutter/networks/base_api_service.dart';
import 'package:get/get.dart' as translator;

class NetworkService extends BaseApiService {
  static final NetworkService _instance = NetworkService._internal();

  factory NetworkService() => _instance;

  NetworkService._internal();

  final _dio = Dio();

  Map<String, dynamic> requestHeaders = {
    'Content-type': 'application/json',
    'platform': Platform.operatingSystem,
    'Accept': 'application/json',
  };

  Options options = Options(validateStatus: (int? status) {
    return status == null ? false : status <= 500;
  });

  @override
  Future get({required String url, required bool isTokenRequired}) async {
    dynamic responseJson;
    if (isTokenRequired) {
      requestHeaders['Authorization'] =
          'Bearer ${PreferenceUtils().getString(PrefConstants.kToken)}';
    }else{
      requestHeaders.remove('Authorization');
      options.headers = requestHeaders;
    }
    options.headers = requestHeaders;
    kLocalLogWriter('GET API CALLED>>>>>>>> $url');
    kPrintWrapped(options.headers.toString());
    try {
      Response response = await _dio.get(url, options: options);
      kLocalLogWriter('>>>>>>>> RESPONSE <<<<<<<<');
      kPrintWrapped(response.data.toString());
      responseJson = returnResponse(response);
    } on SocketException {
      throw InternetException();
    } on RequestTimeOut {
      throw RequestTimeOut();
    } on ServerException {
      throw ServerException();
    }

    return responseJson;
  }

  @override
  Future post(
      {required String url,
      required bool isTokenRequired,
      required Map<String, dynamic> request}) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future put(
      {required String url, required isTokenRequired, required request}) {
    // TODO: implement put
    throw UnimplementedError();
  }

  @override
  Future delete(
      {required String url, required bool isTokenRequired, required request}) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future multiParts(
      {required String url,
      required bool isTokenRequired,
      required Map<String, dynamic> request,
      required int uploadState}) {
    // TODO: implement multiParts
    throw UnimplementedError();
  }

  dynamic returnResponse(Response response) {
    switch (response.statusCode) {
      case kTokenExpired:
        kSnackBar('kLoggedOutTxt'.tr, 'kSessionExpired'.tr);
        kLogout();
        throw UnAuthorizedException();
      default:
        return response.data;
    }
  }
}
