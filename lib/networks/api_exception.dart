class ApiException implements Exception {
  final dynamic _message;
  final dynamic _prefix;

  ApiException([this._message, this._prefix]);

  @override
  String toString() {
    return "$_prefix$_message";
  }
}

class InternetException extends ApiException {
  InternetException([String? message])
      : super(message, 'No internet connection');
}

class RequestTimeOut extends ApiException {
  RequestTimeOut([String? message]) : super(message, 'Request time out');
}

class ServerException extends ApiException {
  ServerException([String? message]) : super(message, 'Internal server error');
}

class FetchDataException extends ApiException {
  FetchDataException([String? message])
      : super(message, 'Error occurred while communicating with server');
}

class URLException extends ApiException {
  URLException([String? message]) : super(message, 'Invalid URl');
}

class UnAuthorizedException extends ApiException {
  UnAuthorizedException([String? message]) : super(message, 'Token is invalid');
}
