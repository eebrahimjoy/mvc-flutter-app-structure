abstract class BaseApiService {
  Future<dynamic> get({required String url, required bool isTokenRequired});

  Future<dynamic> post(
      {required String url,
      required bool isTokenRequired,
      required Map<String, dynamic> request});

  Future<dynamic> put(
      {required String url, required isTokenRequired, required request});

  Future<dynamic> delete(
      {required String url, required bool isTokenRequired, required request});

  Future<dynamic> multiParts(
      {required String url,
      required bool isTokenRequired,
      required Map<String, dynamic> request,
      required int uploadState});
}
