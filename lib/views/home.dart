import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/controllers/home_controller.dart';
import 'package:mvc_structure_flutter/theme/colors.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (homeController) {
        return Scaffold(
          body: homeController.widgetOptions
              .elementAt(homeController.selectedIndex),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: AppColors.kDarkPrimaryColor,
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.menu),
                label: 'Categories',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Me',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart),
                label: 'Cart',
              ),
            ],
            currentIndex: homeController.selectedIndex,
            selectedItemColor: AppColors.kDarkPrimaryColor,
            unselectedItemColor: Colors.grey,
            showUnselectedLabels: true,
            showSelectedLabels: true,
            onTap: (index) {
              homeController.onItemTapped(index);
            },
          ),
        );
      },
    );
  }
}
