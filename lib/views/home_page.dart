import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/controllers/settings_controller.dart';
import 'package:mvc_structure_flutter/sizes/dynamic_sizes.dart';
import 'package:mvc_structure_flutter/theme/colors.dart';

final List<String> imgList = [
  'https://fashionpass.s3.us-west-1.amazonaws.com/assets/nonmember_mainblock_updated.jpg?profile=a',
  'https://fashionpass.s3.us-west-1.amazonaws.com/assets/nonmember_mainblock_updated.jpg?profile=a',
  'https://fashionpass.s3.us-west-1.amazonaws.com/assets/nonmember_mainblock_updated.jpg?profile=a',
];
final List<String> clothTypes = [
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dw0c4e028d/2024/5/CIRCLES_SPORTS_03.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dwef8f2a19/2024/5/CIRCLES_SPORTS_07.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dwf1d2a75b/2024/CIRCLES_07_WINTERWEAR.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dw0c4e028d/2024/5/CIRCLES_SPORTS_03.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dwef8f2a19/2024/5/CIRCLES_SPORTS_07.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dwf1d2a75b/2024/CIRCLES_07_WINTERWEAR.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dwf1d2a75b/2024/CIRCLES_07_WINTERWEAR.jpg',
  'https://en-ae.sssports.com/on/demandware.static/-/Library-Sites-UAE-SharedLibrary/default/dw0c4e028d/2024/5/CIRCLES_SPORTS_03.jpg',
];

class HomePage extends StatelessWidget {
  HomePage({super.key});

  final controller = Get.find<SettingsController>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SettingsController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: controller.backgroundColor,
            elevation: 0,
            leading: Icon(
              Icons.account_circle,
              size: kSearchBoxHeight,
              color: AppColors.kDarkPrimaryColor,
            ),
            title: Row(
              children: [
                Flexible(
                  child: Container(
                    height: kSearchBoxHeight,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(kSmallHeight),
                    ),
                    child: Row(
                      children: [
                        kSmallSizedBoxWidth,
                        Icon(Icons.search, size: kSmallIcon),
                        kSmallSizedBoxWidth,
                        Text(
                          'kHomeSearchTxt'.tr,
                          style: TextStyle(fontSize: kSmallFontSize),
                        ),
                      ],
                    ),
                  ),
                ),
                kMediumSizedBoxWidth,
                const Icon(Icons.favorite_border_outlined),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 12.0, bottom: 4, top: 4),
                  color: controller.backgroundColor,
                  child: Row(
                    children: [
                      const Icon(
                        Icons.location_on,
                        size: 20,
                      ),
                      Text(
                        'kAddressTxt'.tr,
                        style: TextStyle(fontSize: kSmallFontSize),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 70,
                        child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: clothTypes.length,
                          itemBuilder: (context, index) {
                            String url = clothTypes[index];
                            return Column(
                              children: [
                                CircleAvatar(
                                    radius: 24, child: Image.network(url)
                                    // foregroundImage:
                                    //     AssetImage('assets/images/man_style.png'),
                                    ),
                                const Text(
                                  'MEN',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: "monospace",
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                                Visibility(
                                  visible: index == 0,
                                  child: const SizedBox(
                                    height: 2,
                                    width: 40,
                                    child: Divider(),
                                  ),
                                ),
                              ],
                            );
                          },
                          separatorBuilder: (_, __) {
                            return const SizedBox(
                              width: 4,
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 300,
                        child: Image.network(
                            'https://fashionpass.s3.us-west-1.amazonaws.com/assets/nonmember_mainblock_updated.jpg?profile=a'),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'BEST RENTING',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: "monospace",
                            ),
                          ),
                          Text(
                            'See All',
                            style: TextStyle(
                              fontSize: 16,
                              color: AppColors.kDarkPrimaryColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 210,
                        child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: 10,
                          itemBuilder: (_, __) {
                            return InkWell(
                              onTap: () {},
                              child: Column(
                                children: [
                                  Stack(
                                    children: [
                                      Image.network(
                                        'https://images.fashionpass.com/products/aspen-gilet-varley-coffee-bean-954-1.jpg?profile=b2x3',
                                        fit: BoxFit.fitWidth,
                                        height: 150,
                                        // width: 100,
                                      ),
                                      const Positioned(
                                        right: 8,
                                        top: 8,
                                        child: Icon(
                                          Icons.favorite_border_outlined,
                                          size: 20,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const Text(
                                    'RONOCO SAND',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                      fontFamily: "monospace",
                                    ),
                                  ),
                                  const Text(
                                    'Aradia Halter Dress',
                                    style: TextStyle(
                                      fontSize: 10,
                                    ),
                                  ),
                                  const Text(
                                    'AED 10 to rent',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                      fontFamily: "monospace",
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                          separatorBuilder: (_, __) {
                            return const SizedBox(
                              width: 4,
                            );
                          },
                        ),
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'FESTIVAL CLOTHS',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: "monospace",
                            ),
                          ),
                          Text(
                            'See All',
                            style: TextStyle(
                              fontSize: 16,
                              color: AppColors.kDarkPrimaryColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 210,
                        child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: 10,
                          itemBuilder: (_, __) {
                            return InkWell(
                              onTap: () {},
                              child: Column(
                                children: [
                                  Stack(
                                    children: [
                                      Image.network(
                                        'https://images.fashionpass.com/products/jasalina-mini-3-amanda-uprichard-cocoa-brown-714-2.jpeg?profile=a',
                                        fit: BoxFit.fitWidth,
                                        height: 150,
                                        // width: 100,
                                      ),
                                      const Positioned(
                                        right: 8,
                                        top: 8,
                                        child: Icon(
                                          Icons.favorite_border_outlined,
                                          size: 20,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const Text(
                                    'RONOCO SAND',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                      fontFamily: "monospace",
                                    ),
                                  ),
                                  const Text(
                                    'Aradia Halter Dress',
                                    style: TextStyle(
                                      fontSize: 10,
                                    ),
                                  ),
                                  const Text(
                                    'AED 10 to rent',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                      fontFamily: "monospace",
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                          separatorBuilder: (_, __) {
                            return const SizedBox(
                              width: 4,
                            );
                          },
                        ),
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'DEAL UP TO 60%',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: "monospace",
                            ),
                          ),
                          Text(
                            'See All',
                            style: TextStyle(
                              fontSize: 16,
                              color: AppColors.kDarkPrimaryColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 210,
                        child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemCount: 10,
                          itemBuilder: (_, __) {
                            return InkWell(
                              onTap: () {},
                              child: Column(
                                children: [
                                  Stack(
                                    children: [
                                      Image.network(
                                        'https://images.fashionpass.com/products/lenza-gathered-detail-print-blouse-greylin-lipstick-red-0eb-1.jpg?profile=a',
                                        fit: BoxFit.fitWidth,
                                        height: 150,
                                        // width: 100,
                                      ),
                                      const Positioned(
                                        right: 8,
                                        top: 8,
                                        child: Icon(
                                          Icons.favorite_border_outlined,
                                          size: 20,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const Text(
                                    'RONOCO SAND',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                      fontFamily: "monospace",
                                    ),
                                  ),
                                  const Text(
                                    'Aradia Halter Dress',
                                    style: TextStyle(
                                      fontSize: 10,
                                    ),
                                  ),
                                  const Text(
                                    'AED 10 to rent',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        fontFamily: "monospace",
                                        decoration: TextDecoration.lineThrough),
                                  ),
                                  const Text(
                                    'AED 4 to rent',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                      fontFamily: "monospace",
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          separatorBuilder: (_, __) {
                            return const SizedBox(
                              width: 4,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
