import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/controllers/home_controller.dart';

class ListPage extends StatefulWidget {
  const ListPage({super.key});

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  var mHomeController = Get.find<HomeController>();
  static const platform = MethodChannel('samples.flutter.dev/battery');
  String _batteryLevel = 'Unknown battery level.';

  @override
  void initState() {
    super.initState();
    mHomeController.setScrollController();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      mHomeController.fetchQuranEditions();
    });
  }

  Future<void> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final result = await platform.invokeMethod<int>('getBatteryLevel');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }

    setState(() {
      _batteryLevel = batteryLevel;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (homeController) {
        return Scaffold(
          appBar: AppBar(
            title: ElevatedButton(
              onPressed: () {
                _getBatteryLevel();
              },
              child: Text('From Android Native'),
            ),
            actions: [
              Text(_batteryLevel ?? ''),
            ],
          ),
          body: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  controller: homeController.scrollController,
                  itemCount: homeController.listEditions.values.length,
                  itemBuilder: (context, index) {
                    var map =
                        homeController.listEditions.values.elementAt(index);
                    return Text(map['name']);
                  },
                ),
              ),
              Visibility(
                visible: homeController.isLoadMoreRunning == true,
                child: const CircularProgressIndicator(),
              ),
            ],
          ),
        );
      },
    );
  }
}
