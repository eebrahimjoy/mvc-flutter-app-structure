import 'package:get/get.dart';

class AppTranslations extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en': {
          'kLoggedOutTxt': 'Logged Out',
          '≈': 'Session has been expired, please login again',
        },
        'bn': {
          'kLoggedOutTxt': 'লগ আউট',
          'kSessionExpired': 'আপনার সেশন এক্সপাইর্ড হয়ে গেছে, আবার লগইন করুন।',
        },
      };
}
