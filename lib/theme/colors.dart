import 'package:flutter/material.dart';

class AppColors {
  static const Color kPrimaryColor = Color(0xFFFFD700);
  static const Color kBackgroundColor = Color(0xFFFAFAD2);
  static const Color kTintColor = Color(0xFFEEE8AA);
  static const Color kDarkPrimaryColor = Color(0xFFB8860B);
  static const Color kWhiteColor = Color(0xFFFFFFFF);
  static const String defaultLocale = 'en';
  static const String englishLocale = 'en';
  static const String bengaliLocale = 'bn';
}
