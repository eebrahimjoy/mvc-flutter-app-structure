import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/controllers/settings_controller.dart';
import 'constants/app_constants.dart';
import 'helper/bindings.dart';
import 'localization/app_translations.dart';
import 'routes/pages.dart';
import 'views/home.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  AppBindings()
    ..attachController()
    ..initSharePref();
  runApp(const MyTestApp());
}

class MyTestApp extends StatelessWidget {
  const MyTestApp({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<SettingsController>();
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: controller.primaryColor,
      ),
      darkTheme: ThemeData(
        primaryColor: controller.primaryColor,
      ),
      getPages: AppPages.routes,
      locale: const Locale(AppConstants.defaultLocale),
      fallbackLocale: const Locale(AppConstants.defaultLocale),
      translations: AppTranslations(),
      home: const Home(),
    );
  }
}
