import 'package:shared_preferences/shared_preferences.dart';

class PreferenceUtils {
  static final PreferenceUtils _instance = PreferenceUtils._internal();

  factory PreferenceUtils() => _instance;

  PreferenceUtils._internal();

  late SharedPreferences _pref;

  Future<void> init() async {
    _pref = await SharedPreferences.getInstance();
  }

  String getString(String key, [String defValue = '']) {
    return _pref.getString(key) ?? defValue;
  }

  Future<void> setString(String key, String value) async {
    _pref.setString(key, value);
  }

  int getInt(String key, [int defValue = 0]) {
    return _pref.getInt(key) ?? defValue;
  }

  Future<void> setInt(String key, int value) async {
    _pref.setInt(key, value);
  }

  bool getBool(String key, [bool defValue = false]) {
    return _pref.getBool(key) ?? defValue;
  }

  Future<void> setBool(String key, bool value) async {
    _pref.setBool(key, value);
  }

  void clear() {
    _pref.clear();
  }
}
