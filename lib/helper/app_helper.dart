import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/routes/routes.dart';
import 'package:mvc_structure_flutter/theme/colors.dart';

import 'pref_helper.dart';

void kPrintWrapped(String text) {
  // 800 is the size of each chunk
  final pattern = RegExp('.{1,800}');
  pattern.allMatches(text).forEach((match) {
    if (kDebugMode) {
      print(match.group(0));
    }
  });
}

void kLocalLogWriter(dynamic text) {
  if (kDebugMode) {
    print(text);
  }
}

void kSnackBar(dynamic title, dynamic content) {
  Get.snackbar(
    title,
    content,
    backgroundColor: AppColors.kDarkPrimaryColor,
    colorText: AppColors.kWhiteColor,
  );
}

void kProgressLoader() {
  Get.defaultDialog(
    backgroundColor: Colors.transparent,
    barrierDismissible: false,
    content: const CircularProgressIndicator(),
  );
}

void kLogout() {
  PreferenceUtils().clear();
  Get.offAllNamed(Routes.home);
}
