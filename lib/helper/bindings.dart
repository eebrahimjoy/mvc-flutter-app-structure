import 'package:get/get.dart';
import 'package:mvc_structure_flutter/controllers/home_controller.dart';
import 'package:mvc_structure_flutter/controllers/settings_controller.dart';
import 'pref_helper.dart';

class AppBindings {
  static final AppBindings _instance = AppBindings._internal();

  factory AppBindings() => _instance;

  AppBindings._internal();

  void attachController() {
    Get.put(SettingsController());
    Get.put(HomeController());
  }

  Future<void> initSharePref() async => await PreferenceUtils().init();
}
