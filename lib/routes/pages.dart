import 'package:get/get.dart';
import 'package:mvc_structure_flutter/views/home.dart';
import 'package:mvc_structure_flutter/views/home_page.dart';
import 'routes.dart';

class AppPages {
  static final routes = [
    GetPage(
      name: Routes.home,
      page: () => const Home(),
    ),
    GetPage(
      name: Routes.homePage,
      page: () => HomePage(),
    ),
  ];
}
