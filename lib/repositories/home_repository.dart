import 'package:mvc_structure_flutter/constants/api_constants.dart';
import 'package:mvc_structure_flutter/networks/network_service.dart';

class HomeRepository {
  final _networkService = NetworkService();

  Future<dynamic> fetchQuranEditionsAPI() async {
    final response = await _networkService.get(
      url: kEditionsApiEndPoint,
      isTokenRequired: false,
    );
    return response;
  }

// Future<dynamic> postOpenApi() async {
//   final response = await _apiService.get(
//     url: eActiveLocationsAPI,
//     isTokenRequired: true,
//   );
//   return response;
// }
//
// Future<dynamic> deleteOpenApi(int locationId) async {
//   final response = await _apiService.get(
//     url: '$eStatusOverviewAPI?location_id=$locationId',
//     isTokenRequired: true,
//   );
//   return response;
// }
//
// Future<dynamic> uploadFileApi(int locationId) async {
//   final response = await _apiService.get(
//     url: '$eStatusOverviewAPI?location_id=$locationId',
//     isTokenRequired: true,
//   );
//   return response;
// }
}
