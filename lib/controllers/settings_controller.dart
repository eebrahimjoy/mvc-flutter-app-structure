import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/constants/app_constants.dart';
import 'package:mvc_structure_flutter/theme/colors.dart';

class SettingsController extends GetxController {
  final _isDarkMode = false.obs;
  final _primaryColor = AppColors.kPrimaryColor.obs;
  final _backgroundColor = AppColors.kBackgroundColor.obs;

  get isDarkMode => _isDarkMode.value;

  set isDarkMode(value) {
    _isDarkMode.value = value;
  }

  get primaryColor => _primaryColor.value;

  set primaryColor(value) {
    _primaryColor.value = value;
  }

  get backgroundColor => _backgroundColor.value;

  set backgroundColor(value) {
    _backgroundColor.value = value;
  }

  void toggleTheme() {
    isDarkMode = !isDarkMode;
    updateThemeColors();
  }

  void updateThemeColors() {
    if (isDarkMode) {
      primaryColor = AppColors.kPrimaryColor;
      backgroundColor = Colors.yellow;
      Get.changeTheme(ThemeData.dark());
    } else {
      primaryColor = AppColors.kPrimaryColor;
      backgroundColor = Colors.green;
      Get.changeTheme(ThemeData.light());
    }
    update();
  }

  void updateLocale([String langCode = AppConstants.defaultLocale]) {
    Get.updateLocale(Locale(langCode));
  }
}
