import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc_structure_flutter/helper/app_helper.dart';
import 'package:mvc_structure_flutter/repositories/home_repository.dart';
import 'package:mvc_structure_flutter/views/home_page.dart';
import 'package:mvc_structure_flutter/views/list_page.dart';

class HomeController extends GetxController {
  final _homeRepo = HomeRepository();
  final _selectedIndex = 0.obs;
  var _page = 1;
  var _limit = 10;
  bool _hasNextPage = true;
  bool _isFirstLoadRunning = true;
  final _isLoadMoreRunning = false.obs;
  ScrollController scrollController = ScrollController();
  var _totalCounts = 0;
  final _listEditions = {}.obs;

  get listEditions => _listEditions;

  set listEditions(value) {
    _listEditions.value = value;
  }

  set addListEditions(value) {
    _listEditions.addAll(value);
  }

  get totalCounts => _totalCounts;

  set totalCounts(value) {
    _totalCounts = value;
  }

  get page => _page;

  set page(value) {
    _page = value;
  }

  get limit => _limit;

  set limit(value) {
    _limit = value;
  }

  get hasNextPage => _hasNextPage;

  set hasNextPage(value) {
    _hasNextPage = value;
  }

  get isFirstLoadRunning => _isFirstLoadRunning;

  set isFirstLoadRunning(value) {
    _isFirstLoadRunning = value;
  }

  get isLoadMoreRunning => _isLoadMoreRunning.value;

  set isLoadMoreRunning(value) {
    _isLoadMoreRunning.value = value;
  }

  void setScrollController() {
    scrollController.addListener(fetchMoreQuranEditions);
  }

  get disposeScrollController =>
      scrollController.removeListener(fetchMoreQuranEditions);

  get selectedIndex => _selectedIndex.value;

  set selectedIndex(value) {
    _selectedIndex.value = value;
  }

  void onItemTapped(int index) {
    _selectedIndex.value = index;
    update();
  }

  final widgetOptions = [
    HomePage(),
    const ListPage(),
    const Text(
      'Index 3: Settings',
    ),
    const Text(
      'Index 4: School',
    ),
  ];

  Future<void> fetchQuranEditions() async {
    _listEditions.value = {};
    update();
    kProgressLoader();
    try {
      var response = await _homeRepo.fetchQuranEditionsAPI();
      if (response != null) {
        print('ok');
        listEditions = response;
        totalCounts = 100;
      }
    } catch (error) {
      kLocalLogWriter(error.toString());
    } finally {
      Get.back();
      isFirstLoadRunning = false;
      update();
    }
  }

  Future<void> fetchMoreQuranEditions() async {
    final position = scrollController.position;
    if (hasNextPage &&
        !isFirstLoadRunning &&
        !isLoadMoreRunning &&
        position.maxScrollExtent == position.pixels &&
        listEditions.length < totalCounts) {
      isLoadMoreRunning = true;
      page++;
      try {
        var response = await _homeRepo.fetchQuranEditionsAPI();
        if (response != null) {
          if (response != null && response.isNotEmpty) {
            addListEditions = response;
          } else {
            hasNextPage = false;
          }
        }
      } catch (error) {
        kLocalLogWriter(error.toString());
      } finally {
        isLoadMoreRunning = false;
        update();
      }
    }
  }
}
